package com.meatwork.test;

import com.meatwork.tools.service.ServiceManager;

import java.lang.reflect.Field;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
class DefaultMeatworkSession implements MeatworkSession {

    private final Object obj;

    public DefaultMeatworkSession(Object obj) {
        this.obj = obj;
    }

    @Override
    public void inject() throws IllegalAccessException {
        Field[] declaredFields = obj.getClass().getDeclaredFields();

        for (Field declaredField : declaredFields) {
	        if (!declaredField.isAnnotationPresent(InjectTest.class)) {
		        continue;
	        }

	        if(!declaredField.getType().isInterface()) {
		        throw new RuntimeException("%s need to be interface, given class".formatted(declaredField.getName()));
	        }
			declaredField.setAccessible(true);
	        Class<?> type = declaredField.getType();
            declaredField.set(obj, ServiceManager.get(type));
			declaredField.setAccessible(false);
        }
    }
}
