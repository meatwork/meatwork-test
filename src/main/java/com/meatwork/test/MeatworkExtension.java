package com.meatwork.test;

import com.meatwork.tools.configuration.MeatworkConfiguration;
import com.meatwork.tools.event.Component;
import com.meatwork.tools.event.TestModeEnableImpl;
import com.meatwork.tools.model.AbstractEntity;
import com.meatwork.tools.model.Database;
import com.meatwork.tools.model.EntityManager;
import com.meatwork.tools.model.ModelEntity;
import com.meatwork.tools.service.Service;
import com.meatwork.tools.service.ServiceManager;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class MeatworkExtension implements BeforeEachCallback {

    private final List<String> excludePackageName = List.of(
        "org.junit.jupiter.engine.discovery",
        "org.junit.platform.commons.util",
        "org.opentest4j",
        "org.junit.jupiter.engine.discovery.predicates",
        "org.junit.platform.launcher.listeners.discovery",
        "org.junit.jupiter.engine.config",
        "org.junit.platform.commons.support",
        "org.junit.platform.engine.support.store",
        "org.junit.platform.launcher.listeners.session",
        "org.junit.jupiter.engine.support",
        "com.intellij.rt.execution.junit",
        "org.junit.jupiter.engine.descriptor",
        "org.junit.jupiter.engine.execution",
        "com.intellij.rt.junit",
        "org.junit.platform.engine.support.hierarchical",
        "org.junit.platform.commons.function",
        "org.junit.platform.engine.reporting",
        "org.junit.platform.launcher",
        "org.apiguardian.api",
        "org.junit.platform.commons.annotation",
        "org.junit.platform.launcher.core",
        "com.intellij.junit5",
        "org.reflections",
        "org.junit.jupiter.api.io",
        "org.junit.jupiter.engine",
        "org.junit.jupiter.api.parallel",
        "org.junit.platform.commons.logging",
        "org.junit.platform.launcher.listeners",
        "org.junit.platform.engine",
        "org.junit.jupiter.api",
        "org.junit.platform.engine.discovery",
        "org.junit.platform.engine.support.descriptor",
        "org.junit.platform.engine.support.discovery",
        "org.junit.jupiter.engine.extension",
        "org.junit.platform.commons",
        "org.junit.jupiter.api.extension"
    );

    @SuppressWarnings("unchecked")
    public MeatworkExtension() {
        Reflections reflectionsCurrentProject = getRefections(Thread.currentThread().getContextClassLoader());
        Set<Class<Enum<?>>> entities = getByAnnotation(reflectionsCurrentProject, ModelEntity.class)
            .stream()
            .map(it -> (Class<Enum<?>>) it)
            .collect(Collectors.toSet());
        entities.forEach(it ->
            EntityManager.register(
                it,
                () -> (AbstractEntity<?>) newInstance(getEntityImpl(it, reflectionsCurrentProject))
            )
        );
        Set<Class<?>> services = getByAnnotation(reflectionsCurrentProject, ServiceTest.class);
        services.addAll(getByAnnotation(reflectionsCurrentProject, ComponentTest.class));
        services.addAll(getByAnnotation(reflectionsCurrentProject, Component.class));
        Set<Class<?>> classService = getByAnnotation(reflectionsCurrentProject, Service.class);
        Iterator<Class<?>> iterator = classService.iterator();
        while (iterator.hasNext()) {
            Service annotation = iterator.next().getAnnotation(Service.class);
            services.forEach(it -> {
                ServiceTest annotationCurr = it.getAnnotation(ServiceTest.class);
                if (annotationCurr != null && annotationCurr.interfaces().equals(annotation.interfaces())) {
                    iterator.remove();
                }
            });
        }

        services.addAll(classService);
        addComplementaryServices(services);
        services.forEach(it -> {
            Service annotation = it.getAnnotation(Service.class);
            if (annotation != null) {
                ServiceManager.register(annotation.interfaces(), newInstanceService(it, services));
                return;
            }

            ServiceTest annotationTest = it.getAnnotation(ServiceTest.class);
            if (annotationTest != null) {
                ServiceManager.register(annotationTest.interfaces(), newInstanceService(it, services));
            } else {
                ServiceManager.register(it, newInstanceService(it, services));
            }
        });

        Set<Class<?>> mainClassFinded = getByAnnotation(reflectionsCurrentProject, MeatworkConfiguration.class);

        if (mainClassFinded.isEmpty()) {
            throw new IllegalArgumentException("Cannot found main class");
        }

        Class<?> mainClass = mainClassFinded.iterator().next();

        MeatworkConfiguration meatworkConfiguration = mainClass.getAnnotation(MeatworkConfiguration.class);
        if (meatworkConfiguration.enableEntity()) {
            Database.init();
        }
    }

    private void addComplementaryServices(Set<Class<?>> services) {
        services.add(TestModeEnableImpl.class);
    }

    private Object newInstanceService(Class<?> clazz, Set<Class<?>> services) {
        try {
            Constructor<?>[] constructors = clazz.getConstructors();
            Constructor<?> constructor;
            List<Object> args = new ArrayList<>();
            if (constructors.length > 0) {
                constructor = constructors[0];
                Parameter[] parameters = constructor.getParameters();
                for (Parameter parameter : parameters) {
                    Class<?> params = services
                        .stream()
	                    .filter(it -> !it.isAnnotationPresent(Component.class))
                        .filter(it -> {
							Service service = it.getAnnotation(Service.class);
							if(service != null) {
								return it.getAnnotation(Service.class).interfaces().equals(parameter.getType());
							} else {
								return it.getAnnotation(ServiceTest.class).interfaces().equals(parameter.getType());
							}
                        })
                        .findFirst()
                        .orElseThrow(() ->
                            new IllegalArgumentException("cannot find impl of %s".formatted(parameter.getName()))
                        );
                    args.add(newInstanceService(params, services));
                }
            } else {
                constructor = clazz.getConstructor();
            }
            return constructor.newInstance(args.toArray(Object[]::new));
        } catch (
            InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e
        ) {
            throw new RuntimeException(e);
        }
    }

    private Object newInstance(Class<?> clazz) {
        try {
            Constructor<?>[] constructors = clazz.getConstructors();
            Constructor<?> constructor;
            List<Object> args = new ArrayList<>();
            if (constructors.length > 0) {
                constructor = constructors[0];
                Parameter[] parameters = constructor.getParameters();
                for (Parameter parameter : parameters) {
                    args.add(newInstance(parameter.getType()));
                }
            } else {
                constructor = clazz.getConstructor();
            }
            return constructor.newInstance(args.toArray(Object[]::new));
        } catch (
            InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e
        ) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("rawtypes")
    private Class<?> getEntityImpl(Class<?> entity, Reflections... reflections) {
        for (Reflections reflection : reflections) {
            Class<? extends AbstractEntity> aClass = reflection
                .getSubTypesOf(AbstractEntity.class)
                .stream()
                .filter(it ->
                    ((ParameterizedType) it.getGenericSuperclass()).getActualTypeArguments()[0].getTypeName()
                        .equals(entity.getName())
                )
                .findFirst()
                .orElse(null);

            if (aClass != null) {
                return aClass;
            }
        }
        throw new IllegalArgumentException("cannot found impl for entity %s".formatted(entity.getName()));
    }

    private Set<Class<?>> getByAnnotation(Reflections reflections, Class<? extends Annotation> annotation) {
        return reflections.getTypesAnnotatedWith(annotation);
    }

    private Reflections getRefections(Iterator<String> listPackageName) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        while (listPackageName.hasNext()) {
            String packageName = listPackageName.next();
            configurationBuilder.forPackages(packageName).addUrls(ClasspathHelper.forPackage(packageName));
        }
        return new Reflections(configurationBuilder);
    }

    private Reflections getRefections(ClassLoader classLoader) {
        List<String> listPackage = Arrays
            .stream(classLoader.getDefinedPackages())
            .map(Package::getName)
            .filter(it -> !excludePackageName.contains(it))
            .toList();
        return getRefections(listPackage.iterator());
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        List<Object> testInstances = extensionContext.getRequiredTestInstances().getAllInstances();
        MeatworkSession session = Meatwork.getSession(testInstances);
        session.inject();
    }
    //    @Override
    //    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
    //        throws ParameterResolutionException {
    //        return true;
    //    }

    //    @Override
    //    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
    //        throws ParameterResolutionException {
    //        return ServiceManager.get(parameterContext.getParameter().getType());
    //    }
}
