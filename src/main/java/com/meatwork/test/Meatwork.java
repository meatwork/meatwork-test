package com.meatwork.test;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class Meatwork {

	public static MeatworkSession getSession(List<Object> objectList) {
		return new DefaultMeatworkSession(objectList.get(0));
	}

}
