package com.meatwork.test;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
interface MeatworkSession {
    void inject() throws IllegalAccessException;
}
