package com.meatwork.test.entity;

import com.meatwork.tools.api.model.DatabaseConfiguration;
import com.meatwork.tools.service.Service;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Service(interfaces = DatabaseConfiguration.class)
public class DatabaseConfigurationCustom implements DatabaseConfiguration {
	@Override
	public String getUser() {
		return "user";
	}

	@Override
	public String getPassword() {
		return "user";
	}

	@Override
	public String geHost() {
		return "jdbc:h2:mem:test;INIT=CREATE SCHEMA IF NOT EXISTS test";
	}

	@Override
	public String getDriver() {
		return "org.h2.Driver";
	}

	@Override
	public boolean storeInMemory() {
		return false;
	}
}
