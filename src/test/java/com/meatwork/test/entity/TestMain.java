package com.meatwork.test.entity;

import com.meatwork.tools.configuration.MeatworkConfiguration;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@MeatworkConfiguration(enableEntity = true)
public class TestMain {}
