package com.meatwork.test.entity;

import com.meatwork.tools.model.AbstractEntity;
import com.meatwork.tools.model.DataType;
import com.meatwork.tools.model.EntityDelegateImpl;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ParticipantEntity extends AbstractEntity<Participant> {

    public ParticipantEntity() {
        super(new EntityDelegateImpl<>(Participant.class,  TacticalObject.ID.name(), getProperties()));
    }

    public String getName() {
        return (String) entityDelegate.get(TacticalObject.NAME.name());
    }

    public void setName(String name) {
        entityDelegate.put(TacticalObject.NAME.name(), name);
    }

    public String getCode() {
        return (String) entityDelegate.get(TacticalObject.CODE.name());
    }

    public void setCode(String code) {
        entityDelegate.put(TacticalObject.CODE.name(), code);
    }

    public Long getId() {
        return (Long) entityDelegate.get(TacticalObject.ID.name());
    }

    private static Map<String, DataType> getProperties() {
        Map<String, DataType> map = new HashMap<>();
        map.put(TacticalObject.ID.name(), new DataType(TacticalObject.ID.getType(), TacticalObject.ID.getClass().getAnnotations()));
        map.put(TacticalObject.NAME.name(), new DataType(TacticalObject.NAME.getType(), TacticalObject.NAME.getClass().getAnnotations()));
        map.put(TacticalObject.CODE.name(), new DataType(TacticalObject.CODE.getType(), TacticalObject.CODE.getClass().getAnnotations()));
        return map;
    }
}
