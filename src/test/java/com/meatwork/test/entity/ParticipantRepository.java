package com.meatwork.test.entity;

import com.meatwork.tools.model.BaseRepository;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ParticipantRepository extends BaseRepository<Participant, ParticipantEntity> {
	public ParticipantRepository() {
		super(Participant.class);
	}
}
