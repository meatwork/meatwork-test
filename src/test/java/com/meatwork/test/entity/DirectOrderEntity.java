package com.meatwork.test.entity;

import com.meatwork.tools.model.AbstractEntity;
import com.meatwork.tools.model.DataType;
import com.meatwork.tools.model.EntityDelegateImpl;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class DirectOrderEntity extends AbstractEntity<DirectOrder> {

    public DirectOrderEntity() {
        super(new EntityDelegateImpl<>(DirectOrder.class,  TacticalObject.ID.name(), getProperties()));
    }

    public String getName() {
        return (String) entityDelegate.get(TacticalObject.NAME.name());
    }

    public void setName(String name) {
        entityDelegate.put(TacticalObject.NAME.name(), name);
    }

    public String getCode() {
        return (String) entityDelegate.get(TacticalObject.CODE.name());
    }

    public void setCode(String code) {
        entityDelegate.put(TacticalObject.CODE.name(), code);
    }

    public Long getId() {
        return (Long) entityDelegate.get(TacticalObject.ID.name());
    }

    public String getCodeMouvement() {
        return (String) entityDelegate.get(DirectOrder.CODE_MOVEMENT.name());
    }

    public void setCodeMouvement(String codeMouvement) {
        entityDelegate.put(DirectOrder.CODE_MOVEMENT.name(), codeMouvement);
    }

    public Long getParticipant() {
        return (Long) entityDelegate.get(DirectOrder.PARTICIPANT_ID.name());
    }

    public void setParticipant(Long participantId) {
        entityDelegate.put(DirectOrder.PARTICIPANT_ID.name(), participantId);
    }

    private static Map<String, DataType> getProperties() {
        Map<String, DataType> map = new HashMap<>();
        map.put(DirectOrder.CODE_MOVEMENT.name(), new DataType(DirectOrder.CODE_MOVEMENT.getType(), DirectOrder.CODE_MOVEMENT.getClass().getAnnotations()));
        map.put(TacticalObject.ID.name(), new DataType(TacticalObject.ID.getType(), TacticalObject.ID.getClass().getAnnotations()));
        map.put(TacticalObject.NAME.name(), new DataType(TacticalObject.NAME.getType(), TacticalObject.NAME.getClass().getAnnotations()));
        map.put(TacticalObject.CODE.name(), new DataType(TacticalObject.CODE.getType(), TacticalObject.CODE.getClass().getAnnotations()));
        map.put(DirectOrder.PARTICIPANT_ID.name(), new DataType(DirectOrder.PARTICIPANT_ID.getType(), DirectOrder.PARTICIPANT_ID.getClass().getAnnotations()));
        return map;
    }
}
