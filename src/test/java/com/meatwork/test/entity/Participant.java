package com.meatwork.test.entity;

import com.meatwork.tools.model.ModelEntity;
import com.meatwork.tools.model.Property;
import lombok.RequiredArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@ModelEntity(childOf = { TacticalObject.class })
@RequiredArgsConstructor
public enum Participant implements Property {

	;

	private final Class<?> type;

	@Override
	public Class<?> getType() {
		return type;
	}
}
