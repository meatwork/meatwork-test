package com.meatwork.test.entity;

import com.meatwork.tools.model.BaseRepository;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class DirectOrderRepository extends BaseRepository<DirectOrder, DirectOrderEntity> {
	public DirectOrderRepository() {
		super(DirectOrder.class);
	}
}
