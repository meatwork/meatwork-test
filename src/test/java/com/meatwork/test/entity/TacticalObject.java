package com.meatwork.test.entity;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

import com.meatwork.tools.model.Id;
import com.meatwork.tools.model.Property;
import com.meatwork.tools.model.vaildation.Mandatory;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TacticalObject implements Property {
    @Mandatory
    @Id
    ID(Long.class),
    NAME(String.class),
    CODE(String.class);

    private final Class<?> type;

    @Override
    public Class<?> getType() {
        return type;
    }
}
