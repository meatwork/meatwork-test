package com.meatwork.test.entity;

import com.meatwork.test.ServiceTest;
import com.meatwork.tools.api.model.ModelIdentity;
import com.meatwork.tools.model.AbstractEntity;
import com.meatwork.tools.model.BaseRepository;
import com.meatwork.tools.model.EntityDelegate;

import java.util.Set;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@ServiceTest(interfaces = ModelIdentity.class)
public class ModelIdentityImpl implements ModelIdentity {
	@Override
	public Object generateIdentity(EntityDelegate<?> entityDelegate) {
		BaseRepository<?, ? extends AbstractEntity<?>> abstractEntityRepository = new BaseRepository<>(entityDelegate.getType());
		return abstractEntityRepository.getNextSequence();
	}

	@Override
	public Set<Class<? extends Enum<?>>> acceptTypes() {
		return null;
	}
}
