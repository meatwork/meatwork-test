package com.meatwork.test.entity;

import com.meatwork.test.InjectTest;
import com.meatwork.test.MeatworkExtension;
import com.meatwork.tools.model.ValidatorFlow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@ExtendWith(MeatworkExtension.class)
public class EntityDelegateTest {

	@InjectTest
	private AnService anService;

	@Test
    void testEntity() {
        DirectOrderRepository directOrderRepository = new DirectOrderRepository();
        ParticipantEntity participantEntity = new ParticipantEntity();
        participantEntity.setName("participant");

        DirectOrderEntity directOrderEntity = new DirectOrderEntity();
        directOrderEntity.setCode("CODE");
        directOrderEntity.setName("Aname");
        directOrderEntity.setCodeMouvement("Move");

        ParticipantRepository participantRepository = new ParticipantRepository();
        participantRepository.save(participantEntity);
        directOrderEntity.setParticipant(participantEntity.getId());
        ValidatorFlow<DirectOrder> validatorFlow = directOrderRepository.save(directOrderEntity);

        if (validatorFlow.isValid()) {
            directOrderEntity = directOrderRepository.findById(1);
            directOrderEntity.setCode("POPP");
            directOrderRepository.save(directOrderEntity);
            Long participantId = directOrderEntity.getParticipant();
            ParticipantEntity participant = participantRepository.findById(participantId);
            Assertions.assertEquals(1, directOrderEntity.getId());
            Assertions.assertEquals("POPP",directOrderEntity.getCode());
            Assertions.assertEquals("participant",participant.getName());
        } else {
            String format = validatorFlow
                .errors()
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + " : " + entry.getValue())
                .collect(Collectors.joining("\n"));
            throw new IllegalArgumentException("error : %s".formatted(format));
        }
		Assertions.assertNotNull(anService);
    }
}
