package com.meatwork.test.entity;

import com.meatwork.tools.model.ModelEntity;
import com.meatwork.tools.model.Property;
import lombok.RequiredArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@ModelEntity(childOf = { TacticalObject.class })
@RequiredArgsConstructor
public enum DirectOrder implements Property {

	CODE_MOVEMENT(String.class),
	PARTICIPANT_ID(Long.class)

	;

	private final Class<?> type;

	@Override
	public Class<?> getType() {
		return type;
	}
}
