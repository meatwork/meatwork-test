package com.meatwork.test.entity;

import com.meatwork.tools.api.model.DatabaseConfiguration;
import com.meatwork.tools.service.Inject;
import com.meatwork.tools.service.Service;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Service(interfaces = AnService.class)
public class AnServiceImpl implements AnService {

	private final DatabaseConfiguration databaseConfiguration;

	@Inject
	public AnServiceImpl(DatabaseConfiguration databaseConfiguration) {
		this.databaseConfiguration = databaseConfiguration;
	}
}
